\documentclass[fleqn,10pt]{wlscirep}

\usepackage[ style=authortitle,
backend=bibtex,
sorting=nyt,
sortcase=false,
sortcites=true,
maxnames=3,
maxbibnames=3,
hyperref=true,
abbreviate=true,
date=year,
dashed=false ]{biblatex}

\addbibresource{BarryMuldrey.bib}

\title{Statement of Research Interests}

\author{Barry J. Muldrey, Ph.D. Candidate}
\affil{College of  Electrical and Computer Engineering,
	Georgia Institute of Technology
	Atlanta, GA, 30332, USA}
%\affil[2]{Affiliation, department, city, postcode, country}
%\affil[*]{corresponding.author@email.example}
%\affil[+]{these authors contributed equally to this work}

%\keywords{Keyword1, Keyword2, Keyword3}

\begin{abstract}
Over the course of my Ph.D., I have worked in various regions near the 
intersection of circuits and machine learning focusing on the verification of 
system designs and the validation of fabricated systems.
I am intrigued by the role complexity plays as both the source of emergent 
beauty and utility and as an agent of frustration.
I have become literate in adjacent disciplines that I find both fascinating 
and which strike a harmony with the underlying themes of my work; for 
example: system identification, nonparametric statistics, topology, and 
nonlinear dynamics.
In the near term, I'm interested in various unexplored analog PUF 
(physically unclonable function) circuit structures, 
I'm interested in the use of GANs (generative adversarial networks) as a 
means of accelerating 
the identification and exploration of high-risk corners of the state-spaces 
of autonomous systems,
and I'm interested in the continued pursuit of scalable automated tools for 
debugging large and complex systems.

\end{abstract}

\begin{document}
\flushbottom
\maketitle
%\thispagestyle{empty}

\section*{Introduction}

At the conclusion of my PhD research, I find myself at the vertex where the 
arts of mixed-signal circuit design and CMOS/MEMS microfabrication abut 
the fields of system testing and diagnosis, system reliability, and machine 
learning.
Upon reflection, it is \textit{complexity}, that I find to be a  common thread 
peeking out in various ways along my path.
A great deal of engineering as a discipline has sought its way though 
jungles of complexity by yielding tools of abstraction.
The concept of abstraction is possibly the greatest tool known to electrical 
engineering or perhaps, even to science more broadly.
It provides a mechanism for complexity reduction; we waive away 
high-order 
effects without risk under certain conditions and design structures which 
render them negligible.
It's a wonderful analytical tool as well; for example, the abstraction of the 
very complex dynamical system 
comprised of transistors and the subsequent substitution of boolean 
arithmetic units is the fundamental abstraction enabling the remarkable 
computational ability humans presently have at their disposal.

My work in post-silicon validation and design verification is a study of the 
high-order effects;
when their contribution to system dynamics begins to leave the realm of 
negligible, entering significance.
Here, circuits have to be validated and functionally verified.
The analytical tools we have to deal with abstractions can fail us when our 
assumptions don't hold, and our ability to deal with and draw inferences 
from 
high-order complexity is nonexistent at worst, and grossly inefficient at 
best.
When we fabricate transistors that cease to behave 
deterministically, the assumptions underlying our abstractions fail,
and our abstract models cease to provide meaningful predictions.
The tools remaining are ODE solvers; excellent for predicting behaviors of 
high-order models, but impotent in the context of hundreds of thousands to 
billions of devices.


\section*{Major Trajectories of Doctoral Work}
Early at Georgia Tech, I built a hardware and software 
framework implementing a GA (genetic algorithm) for the exploration of the 
behaviors of circuit pairs in an adversarial manner; maximizing a fitness 
based on observed difference in the frequency-domain magnitude 
response. This work was well received and I turned my attention to what 
should be done after behavioral discrepancies are revealed.

Thus, in the next portion of work, I implemented a framework 
which implements a ``sparse Weiner network,'' a compact neural structure 
of my own design, to be deployed a few neurons at a time in the interior of 
dynamical circuit models. The network is trained incrementally through 
forward simulation only, allowing for noninvertible blocks to exist 
between the network output and observable nodes of the circuit.

For the next subject of study, I examined \textit{the locations} of the 
embedding of the learning elements inside larger models. I built a 
framework for automated experimentation with the location of the learning 
element. I presented an algorithm to predict faulty components within large 
nonlinear systems through the discovery of divergence, the 
construction and subsequent fitting of many hypothetical models, and 
evaluation through a final round of divergence discovery.

Most recently, I've looked at RL (reinforcement learning) as a candidate 
approach for exploring high-dimensional system state-spaces, identifying 
and addressing some of the trade-offs and advantages inherent in 
contemporary RL algorithms as applied to circuit validation.
Broadly, my contribution to circuit validation and verification has been to 
adversarially employ a ML (machine learning) actor who is rewarded for 
identifying behavioral divergence and a neural approximator who is 
rewarded for emulating the buggy behavior.

%\pagebreak
\section*{Minor Trajectories of Doctoral Work}

I explored the application of my other techniques to the purpose of 
detecting the presence of maliciously injected trojan-horse circuitry.
In this work, I defined a process which renders a maximal number of 
internal nodes of the circuit simultaneously sensitive to variation in 
capacitance while also stimulating the nodes and propagating that 
information to primary outputs.

With colleagues, I explored techniques for the identification of digital 
trojan-horse circuitry. We implemented a technique whereby pulses of 
decreasing duration are launched through a path in a logical block. A pulse 
of marginally sufficient width will be regenerated by the logical gates and 
will propagate to an output. The presence of additional circuitry would 
superficially diminish the pulse, decreasing its likelihood of propagating. 
The technique thus correlates the observed threshold in successful 
propagation to the likelihood of malicious injected circuitry.

I also worked for some time with colleagues on analog PUFs (physically 
unclonable functions) with the goal of designing a circuit which is rendered 
universally unique through subtle variations in the fabrication process.
Digital versions of such circuits have been proposed (some are in use) but 
forgo the dynamic range and subtlety of variation that analog circuits 
provide.
In this work, we devised a pair of highly nonlinear amplifiers whose power 
supplies are continually varied by the input ``challenge,'' and whose 
individual outputs are continually compared to generate a primary output, 
or ``response.''

\section*{Directions of Ongoing Work}

\begin{enumerate}
	\item  I'm still fascinated by the prospect of analog PUFs and presume 
	that the solution lies in the construction of systems whose dynamics are 
	highly bifurcated.
	In such cases, the parameters of the system can be widely known, but 
	would be difficult to simulate.
	Even simulations with the highest precision and finest stepping control 
	would yield accurate results only to a finite time horizon.
	
	\item I'm interested in the use of GANs as a means of accelerating the 
	identification and exploration of high-risk 
	corners of the state-spaces	of autonomous systems.
	A central trade-off at play in autonomous learning is that between 
	exploration and exploitation.
	But, all too often, there are risks inherent in exploration: risks of loss of 
	valuable equipment, risks of mission failure, risks of loss-of-life.
	Thus, there are compelling reasons to integrate notions of risk into the 
	decision to explore.
	But before an autonomous system can integrate risk, it must 
	autonomously recognize risk, even at first exposure.
	I would like to pursue the use of GANs as a mechanism for synthesizing 
	regions of the state-space, which can be explored (in a limited sense) in 
	order to asses risk.
	
	\item I'm interested in the continued pursuit of scalable automated tools 
	for debugging large and complex systems.
	Evidence suggests that the complexity of man-made systems is a 
	monotonically increasing phenomenon.
	Thus, tools and techniques are required for dealing with this complexity 
	and identifying errors and risks in a timely manner.
	I intend to continue to pursue the application of automation 
	experimentation into a complete and hierarchical framework which can 
	be scaled across circuits, automobiles, and networks.
	
\end{enumerate}

\section*{Notable Publications}
\printbibliography[heading=none]


\end{document}